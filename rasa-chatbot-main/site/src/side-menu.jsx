import React from "react";
import Setting from './assets/setting.svg';
import Subscriber from './assets/subscriber.svg';
import New from './assets/new-indicator-svgrepo-com.svg';
import Explore from './assets/search-alt-svgrepo-com.svg';
import SendFeedback from './assets/send-alt-1-svgrepo-com.svg';
import { Modal, Button } from 'antd'

import { reset } from "./redux/action";
import { useDispatch } from "react-redux";
import { ExclamationCircleFilled } from '@ant-design/icons';
import './index.scss'

const { confirm } = Modal

const SideMenu = () => {

    const [isOpen,setIsOpen] = React.useState(false)

    const dispatch = useDispatch()

    const openModal = React.useCallback(() => {
        setIsOpen(!isOpen)
    }, [isOpen])

    const deleteChat = React.useCallback(() => {
        dispatch(reset())
        setIsOpen(!isOpen)
    }, [isOpen])

    return (
        <div className="w-full h-screen border-r-2 overflow-y-scroll">
            <Modal open={isOpen} onCancel={openModal} centered footer={false} title="This will reset all chat history. Do you want to delete?">
                <div className="flex pt-4 justify-center space-x-8">
                    <Button onClick={openModal} className="" size="large">Cancel</Button>
                    <Button onClick={deleteChat} className="bg-blue-400 text-white" rootClassName="hover:text-white" size="large">Confirm</Button>
                </div>
            </Modal>
            <menu className="h-screen flex flex-col">
                <section className="h-14 flex items-center border-t-2 bg-[#f2e9e9]">
                    {/* <a href="/chatbot"/> */}
                    <span className="w-full text-2xl font-bold text-center">
                        PiBot
                    </span>
                </section>
                <section className="h-20 flex justify-center items-center border-t-2 space-x-4">
                    {/* <a href="/chatbot"/> */}
                    <a href="https://portal.ptit.edu.vn/" target="_blank" className="flex items-center justify-center space-x-2 block w-2/5 h-3/5 border rounded-lg bg-[#f7f7f7] cursor-pointer">
                        <img src={Explore} alt="" className="w-5 h-5"/>
                        <div className="font-[500]">Explore</div>
                    </a>
                    <div onClick={openModal} className="flex justify-center items-center w-2/5 h-3/5 border rounded-lg bg-[#f7f7f7] cursor-pointer">
                        <img src={New} alt="" className="w-6 h-6"/>
                        <div className="font-[500]">New Chat</div>
                    </div>
                </section>
                <section className="h-16 flex items-center border-t-2">
                    {/* <a href="/chatbot"/> */}
                    <div className="flex w-full space-x-3 items-center ml-3">
                        <img    
                            src="https://slink.ptit.edu.vn/favicon.ico" 
                            alt="" 
                            className="h-10 w-10"
                        />
                        <div className="flex flex-col w-full">
                            <div className="flex w-full justify-between pr-3">
                                <span className="font-bold">Bot tư vấn tuyển sinh</span>
                                <span className="flex" style={{ color: "#5d6565" }}>{`>`}</span>
                            </div>
                            <span className="text-green-600">Online</span>
                        </div>
                    </div>
                </section>
                {/* <section className="h-16 flex items-center border-t-2 cursor-pointer hover:bg-[#f7f7f7]">
                    <div className="flex w-full items-center text-center space-x-4 ml-3">
                        <img src={Subscriber} alt="" className="w-10 h-10"/>
                        <div className="font-[450] text-[20px]">Subscribe</div>
                    </div>
                </section>
                <section className="h-16 flex items-center border-t-2 py-4 cursor-pointer hover:bg-[#f7f7f7]">
                    <div className="flex w-full text-2xl text-center space-x-5 ml-4">
                        <img src={Setting} alt="" className="w-8 h-8"/>
                        <span className="font-[450] text-[20px]">Setting</span>
                    </div>
                </section>
                <section className="h-16 flex items-center border-t-2 py-4 cursor-pointer hover:bg-[#f7f7f7] pr-3">
                    <div className="flex w-full text-2xl items-center text-center space-x-5 ml-4">
                        <img src={SendFeedback} alt="" className="w-8 h-8"/>
                        <span className="font-[450] text-[20px]">Send Feedback</span>
                    </div>
                </section> */}
                <section className="grow flex flex-col justify-end border-t-2 py-8">
                    <div className="flex flex-col items-center space-y-4">
                        {/* <button className="border-2 rounded-full p-2 w-3/4 bg-[#f7f7f7]">Download on IOS</button> */}
                        {/* <button className="border-2 rounded-full p-2 w-3/4 bg-[#f7f7f7]">Download on Android</button> */}
                        <div className="flex items-center">Follow us on <a href="https://portal.ptit.edu.vn/" target="_blank"><img src="https://slink.ptit.edu.vn/favicon.ico" className="cursor-pointer w-8 h-8"/></a> </div>
                    </div>
                </section>
                <section className=" items-center pt-4 pb-6 cursor-pointer">
                    <span className="px-4">
                        <a href="" className="hover:underline">About</a>
                        &nbsp;
                         · 
                        &nbsp;
                        <a href="" className="hover:underline">Help center</a>
                        &nbsp;
                         · 
                        &nbsp;
                        <a href="" className="hover:underline">Privacy policy</a>
                        &nbsp;
                         · 
                        &nbsp;
                        <a href="" className="hover:underline">Terms of service</a>
                        &nbsp;
                         · 
                        &nbsp;
                        <a href="" className="hover:underline">Careers</a>
                        {/* About · Help center · Privacy policy · Terms of service · Careers */}
                        {/* <span className="px-1 hover:underline underline-offset-1">About</span>
                        <span className="px-1 hover:underline underline-offset-1">·</span>
                        <span className="px-1 hover:underline underline-offset-1">Help center</span>
                        <span className="px-1 hover:underline underline-offset-1">·</span>
                        <span className="px-1 hover:underline underline-offset-1">Privacy policy</span>
                        <span className="px-1 hover:underline underline-offset-1">·</span>
                        <span className="px-1 hover:underline underline-offset-1">Terms of service</span>
                        <span className="px-1 hover:underline underline-offset-1">·</span>
                        <span className="px-1 hover:underline underline-offset-1">Careers</span> */}
                    </span>
                </section>
            </menu>
        </div>
    )
}

export default React.memo(SideMenu)