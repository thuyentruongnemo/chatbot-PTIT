import React, { useEffect, useLayoutEffect } from "react";
import Micro from './assets/mic.svg';
import Send from './assets/next.svg';
import File from './assets/attach-file.svg';
import Share from './assets/share.svg';

import { normal, reset } from "./redux/action";
import { useSelector, useDispatch } from "react-redux";
import { Drawer } from 'antd'

import './index.scss'

import sideMenu from "./side-menu";

function getSessionId() {
    const storage = localStorage;
    const storageKey = 'RASA_SESSION_ID';
    const savedId = storage.getItem(storageKey);
    if (savedId) {
        return savedId;
    }
    const newId = socket.id;
    storage.setItem(storageKey, newId);
    return newId;
}

import { io } from 'socket.io-client';
import SideMenu from "./side-menu";
const socket = io('http://localhost:5005', { transports: ['websocket'] , path: '/socket.io'});

socket.on("connect", () => {
    console.log('Connected to the server');
    console.log(socket.id)
    socket.emit('session_request', {
        'session_id': getSessionId(),
    });
});

const Chat = () => {

    const [data, setData] = React.useState([])

    const [message, setMessage] = React.useState("")

    const messageRef = React.useRef(null)

    const containerRef = React.useRef(null)

    const [isOpen, setIsOpen] = React.useState(false)

    const isReset = useSelector(state => state)

    const dispatch = useDispatch()

    let isListening = false

    useLayoutEffect(() => {
        setData([])
        dispatch(normal())
    }, [isReset])

    useLayoutEffect(() => {
        console.log('hello socket')
        if(!isListening) {
            socket.on('bot_uttered', async (response) =>{
                setData((oldState) => {
                    return [...oldState, {message: response.text || '', attach: response.attachment?.payload.src? response.attachment.payload.src: '' , pos: "left"}]
                })
            });
            isListening = true
        }
    }, [socket])

    const submit = React.useCallback  (async (event) => {
        if(event.target.value.includes("\n")) {
            const mess = event.target.value.replace("\n","")
            if(mess.length > 0) {
                await socket.emit('user_uttered', {
                    "message": mess,
                    'session_id': getSessionId(),
                });
                setData((oldState) => {
                    return [...oldState, {message: mess, pos: "right"}]
                })
                setMessage("")
            }
        } else {
            setMessage(event.target.value)
        }
    }, [data, setData, message])

    const submitBtn = React.useCallback(async () => {
        if(message.length > 0)
        {
            await socket.emit('user_uttered', {
                "message": message,
                'session_id': getSessionId(),
            });
            setData((oldState) => {
                return [...oldState, {message: message, pos: "right"}]
            })
            setMessage("")
            messageRef.current.focus()
        }
    }, [data, message])

    React.useEffect(() => {
        if(containerRef.current) {
            containerRef.current.scrollTop = containerRef.current.scrollHeight
        }
    }, [data, message, containerRef])

    console.log(data)

    const openDrawer = () => {
        setIsOpen(!isOpen)
    }

    return (
        <>
            <div className="h-full">
                <Drawer open={isOpen} width={300} onClose={openDrawer} placement="left" rootClassName="p-0">
                    <SideMenu/>
                </Drawer>
                <div className="h-14 bg-[#f2e9e9] border-t-2 flex items-center pl-3 justify-between pr-5">
                    <div className="flex space-x-3 items-center">
                        <div onClick={openDrawer} className="flex flex-col items-center justify-center space-y-1 w-8 h-8 rounded-[100%] hover:bg-slate-200 md:hidden">
                            <div className="w-4 h-1 bg-slate-500 rounded-lg"></div>
                            <div className="w-4 h-1 bg-slate-500 rounded-lg"></div>
                            <div className="w-4 h-1 bg-slate-500 rounded-lg"></div>
                        </div>
                        <img
                            src="https://slink.ptit.edu.vn/favicon.ico"
                            alt=""
                            className="h-10 w-10"
                        />
                        <div className="flex flex-col">
                            <span className="font-bold">Bot tư vấn tuyển sinh</span>
                            <span className="text-green-600">Online</span>
                        </div>
                    </div>
                    <button className="flex border-2 rounded-[40px] w-20 h-8 space-x-1 items-center justify-center">
                        <img src={Share} alt="" className="w-[20px] h-[20px]"/>
                        <span>Share</span>
                    </button>
                </div>
                <div ref={containerRef} className="h-[82vh] border-t-2 overflow-y-scroll flex flex-col items-center">
                    <div className="max-w-3/4 w-3/4">
                        <div className="h-fit pb-4 w-full h-36 border rounded-lg bg-slate-100 mt-6 flex flex-col pl-6 space-y-1 pt-4">
                            <div className="flex space-x-4 items-center">
                                <img
                                    src="https://slink.ptit.edu.vn/favicon.ico"
                                    alt=""
                                    className="h-20 w-20"
                                />
                                <div className="flex flex-col">
                                    <span className="text-2xl font-bold">Chào mừng đến với bot tuyển sinh PTIT</span>
                                    <span>Operated by nhom 5</span>
                                </div>
                            </div>
                            {/* <div className="text-[16px] font-[500]">Tư vấn tuyển sinh PTIT</div> */}
                        </div>
                    </div>

                    <div className="w-3/4 mb-2">
                        {/* <WaitingResponse></WaitingResponse> */}
                        {data.map((mess,index) => {
                            if(mess.pos === "right") {
                                return (
                                    <div key={index} className="w-full flex justify-end">
                                        <p style={{ wordBreak: "break-all" }} className="max-w-[70%] w-fit border rounded-lg bg-[#3b3abe] text-white px-3 py-1 mt-3">{mess.message}</p>
                                    </div>
                                )
                            } else {
                                return (
                                    <div key={index} className="w-full flex flex-col pt-3 space-y-1">
                                        <div className="flex items-end space-x-2 font-bold">
                                            <img
                                                src="https://code.ptit.edu.vn/2020/images/logo_ptit.png"
                                                alt=""
                                                className="h-7 w-6"
                                            />
                                            <span>Our Bot</span>
                                        </div>
                                        {mess.message && (
                                            <div className="w-fit border rounded-lg bg-[#f7f7f7] px-3 py-1" style={{ whiteSpace: "pre-line" }}>
                                                {mess.message}
                                            </div>
                                        )}
                                        {mess.attach  && <img  src={mess.attach} />}
                                    </div>
                                )
                            }
                        })}
                    </div>

                </div>
                <div className="border-t-2 flex justify-around">
                    <div className="border-2 border-slate-300 w-[90%] md:w-3/4 h-[3rem] rounded-[40px] mt-3 flex justify-between space-x-4 items-center pl-4">
                        <div className="w-3/4 flex h-full items-center">
                            <textarea ref={messageRef} value={message} onChange={submit} rows={1} cols={30} style={{ resize: "none", overflow: "hidden", whiteSpace: "nowrap" }} className="none w-full h-[2.5rem] pl-3 align-middle leading-[2.5rem] focus: outline-none" placeholder="Talk to our bot"></textarea>
                        </div>
                        <div className="flex justify-end items-center space-x-2 md:space-x-6">
                            <button className="pl-4">
                                <img src={File} alt="" className="w-[50px] h-[50px] md:w-[25px] md:h-[25px]"/>
                            </button>
                            <button>
                                <img src={Micro} alt="" className="w-[50px] h-[50px] md:w-[25px] md:h-[25px]"/>
                            </button>
                            <div onClick={submitBtn} className="flex justify-center items-center bg-blue-400 w-[55px] h-[40px] rounded-[100%] md:w-[45px] md:h-[45px] border">
                                <img src={Send} alt="" className="w-[20px] h-[20px] md:w-[25px] md:h-[25px]"/>
                            </div>
                        </div>
                        
                    </div>
                    {/* <input className="border border-2 border-slate-300 rounded-[40px] w-[40rem] h-[3rem] mt-3 pl-3" placeholder="Talk to our bot"/> */}
                </div>
            </div>
        </>
    )
}

export default React.memo(Chat)
