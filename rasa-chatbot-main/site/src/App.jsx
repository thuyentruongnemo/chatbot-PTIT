import './App.css'
import React from 'react'
import SideMenu from './side-menu'
import Details from './details'
import Chat from './chat-section'
import { resetReducer } from './redux/reducer'
import { Provider } from 'react-redux'

import { createStore } from 'redux';

function App() {

  const store = createStore(resetReducer)

  return (
    <Provider store={store}>
      <div className='flex w-screen h-screen'>
        <div className='hidden md:block md:w-1/5'>
          <SideMenu/>
        </div>
        <div className='w-full md:w-4/5'>
          <Chat/>
        </div>
        {/* <div className='w-2/5 md:w-1/4'>
          <Details/>
        </div> */}
      </div>
    </Provider>
  )
}

export default React.memo(App)
