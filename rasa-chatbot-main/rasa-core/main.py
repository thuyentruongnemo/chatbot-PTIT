import socketio

sio = socketio.Client()


@sio.event
def connect():
    print('Connected to the server')



@sio.on('bot_uttered')
def handle_message(data):
    print('Received message:', data)

# Define an event handler for the 'disconnect' event
@sio.event
def disconnect():
    print('Disconnected from the server')

if __name__ == '__main__':
    server_url = 'http://localhost:5005'

    sio.connect(server_url)

    try:
        while True:
            message = input('Enter a message (or type "exit" to quit): ')
            if message.lower() == 'exit':
                break
            data = {
                'message': message,
            }

            # Send the message to the server
            sio.emit('user_uttered', data)
    except KeyboardInterrupt:
        pass

    # Disconnect from the server
    sio.disconnect()
