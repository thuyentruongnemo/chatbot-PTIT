import unicodedata
import os


def get_root_path():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.dirname(dir_path)


def convert_lowercase(input_string):
    lowercase_string = input_string.lower()
    no_whitespace_string = ''.join(lowercase_string.split())
    no_diacritics_string = ''.join(
        c for c in unicodedata.normalize('NFD', no_whitespace_string) if unicodedata.category(c) != 'Mn')

    return no_diacritics_string

