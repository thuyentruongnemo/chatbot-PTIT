from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.events import AllSlotsReset
from rasa_sdk.executor import CollectingDispatcher
from actions.utils import convert_lowercase, get_root_path
import json
from datetime import datetime

root_path = get_root_path()
data_path = root_path + "/crawl-data/data.json"
bvh_data_path = root_path + "/crawl-data/bvh.json"
bvs_data_path = root_path + "/crawl-data/bvs.json"

with open(data_path, 'r', encoding='utf8') as file:
    data = json.load(file)
with open(bvh_data_path, 'r', encoding='utf8') as file:
    bvh_data = json.load(file)
with open(bvs_data_path, 'r', encoding='utf8') as file:
    bvs_data = json.load(file)


def find_course(course):
    matching_courses = [c for c in data["nganhDaoTao"] if
                       convert_lowercase(course) == convert_lowercase(c.get("tenNganh"))]

    if len(matching_courses) > 0:
        return matching_courses[0]

    return None


def find_course_by_training_facilities(course, training_facilities):

    if "mienbac" in convert_lowercase(training_facilities):
        target_data = bvh_data
    elif "miennam" in convert_lowercase(training_facilities):
        target_data = bvs_data

    matching_courses = [c for c in target_data["data"] if
                       convert_lowercase(course) == convert_lowercase(c.get("nganhHoc").get("tenNganh"))]

    if len(matching_courses) > 0:
        return matching_courses[0]

    return None


class ActionGetCourseInfo(Action):
    def name(self) -> Text:
        return "action_get_course_info"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        course = tracker.get_slot("course")

        if course is not None:
            matching_course = find_course(course)


            if matching_course is not None:
                code = matching_course.get("maNganh")
                training_time = matching_course.get("thoiGianDaoTao")
                semester_start = matching_course.get("kyNhapHoc")
                training_facility = matching_course.get("coSo")
                over_view = matching_course.get("tongQuan")
                total_credits = matching_course.get("tongTinChi")
                tuition_course = matching_course.get("hocPhi")
                subject_combination_list = matching_course.get("toHop")
                subject_combination_str = ", ".join(subject_combination_list)

                dispatcher.utter_message(text=f"Tôi có thể cung cấp cho bạn một số thông tin về ngành {course} như sau: \n Mã ngành học: {code} \n Thời gian đào tạo: {training_time}  \n Kỳ nhập học: {semester_start} \n Cơ sở đào tạo: {training_facility} \n Khối lượng chương trình: {total_credits} \n Học phí: {tuition_course} \nTổ hợp xét tuyển: {subject_combination_str}")
                if over_view:
                    dispatcher.utter_message(text=over_view)
            else:
                dispatcher.utter_message(text="Không tìm thấy ngành học phù hợp")
        else:
            dispatcher.utter_message(text="Không tìm thấy ngành học phù hợp")

        return [AllSlotsReset()]


class ActionGetTuiTionCourse(Action):
    def name(self) -> Text:
        return "action_get_tuition_course"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Học phí dự kiến với sinh viên chính quy. \n Học phí trình độ đại học hệ chính quy chương trình đại trà năm học 2023-2024: trung bình từ khoảng 24,5 triệu đồng đến 27,8 triệu đồng/năm tùy theo từng ngành học; \n Lộ trình tăng học phí tối đa cho từng năm: Mức học phí được điều chỉnh theo lộ trình phù hợp tương xứng với chất lượng đào tạo và đảm bảo tỷ lệ tăng không quá 15%/năm (theo Nghị định 81/2021/NĐCP của Chính phủ).")
        dispatcher.utter_message(text=f"Tham khảo chi tiết tại: https://portal.ptit.edu.vn/giaovu/wp-content/uploads/2023/10/THÔNG-BÁO-THU-HỌC-PHÍ-KỲ-1-NĂM-23-24.pdf")
        return [AllSlotsReset()]


class ActionGetSubjectCombination(Action):

    def name(self) -> Text:
        return "action_get_subject_combination"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        course = tracker.get_slot("course")

        if course is not None:
            matching_course = find_course(course)

            if matching_course is not None:
                subject_combination_list = matching_course.get("toHop")
                subject_combination_str = ", ".join(subject_combination_list)
                dispatcher.utter_message(text=f"Tổ hợp xét tuyển của ngành {course} là : {subject_combination_str}")
            else:
                dispatcher.utter_message(text="Không tìm thấy ngành học phù hợp")
        else:
            dispatcher.utter_message(text="Không tìm thấy ngành học phù hợp")

        return [AllSlotsReset()]


class ActionAdmissionScore(Action):
    def name(self) -> Text:
        return "action_inform_admission_score"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        admission_scores = [
            {
                "text": "Điểm trúng tuyển vào đại học chính quy theo phương thức xét tuyển dựa trên kết quả thi tốt nghiệp THPT 2023 Học Viện Công Nghệ Bưu Chính Viễn Thông - PTIT phía Bắc",
                "image": "https://chatbotdata.aisenote.com/media/images/369651849_689625173207811_4428631318454178542_n.jpg"
            },
            {
                "text": "Điểm trúng tuyển vào đại học chính quy theo phương thức xét tuyển dựa trên kết quả thi tốt nghiệp THPT 2023 Học Viện Công Nghệ Bưu Chính Viễn Thông - PTIT phía Nam",
                "image": "https://chatbotdata.aisenote.com/media/images/369271437_689701329866862_1206628029819311265_n.jpg"
            },
            {
                "text": "Điểm trúng tuyển 2021-2022 Học Viện Công Nghệ Bưu Chính Viễn Thông - PTIT",
                "image": "https://chatbotdata.aisenote.com/media/images/341025825_1610885969383442_6073688512933971818_n_nQOAMvN.png"
            }
        ]

        for score in admission_scores:
            dispatcher.utter_message(text=score["text"], image=score["image"])

        return [AllSlotsReset()]


class ActionAdmissionsCriteria(Action):
    def name(self) -> Text:
        return "action_admissions_criteria"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        course = tracker.get_slot("course")

        if course is not None:
            training_facilities = tracker.get_slot('training_facilities')

            matching_course = find_course(course)

            if matching_course is not None and training_facilities is not None:
                course_data = find_course_by_training_facilities(course, training_facilities)
                if course_data is not None:
                    code = course_data.get("maNganh")
                    diemSanTheoPhuongThucKetHop = course_data.get("diemSanTheoPhuongThucKetHop")
                    chiTieuTheoKetQuaThiTHPT = course_data.get("chiTieuTheoKetQuaThiTHPT")
                    chiTieuTheoPhuongThucKetHop = course_data.get("chiTieuTheoPhuongThucKetHop")
                    chiTieuTheoDGNL = course_data.get("chiTieuTheoDGNL")
                    dispatcher.utter_message(text=f"Theo thông tin chính thống của học viện thì chỉ tiêu tuyển sinh của ngành {course}: \n Chỉ tiêu theo KQ thi THPT: {chiTieuTheoKetQuaThiTHPT} \n Chỉ tiêu theo phương thức kết hợp: {chiTieuTheoPhuongThucKetHop} \n Chỉ tiêu theo đánh giá năng lực: {chiTieuTheoDGNL}  \n Mã ngành: {code} \n Điểm sàn theo phương thức kết hợp:{diemSanTheoPhuongThucKetHop}")
                    return [AllSlotsReset()]

        dispatcher.utter_message(text="Tổng chỉ tiêu là 4.280, trong đó dự kiến chỉ tiêu của 02 Cơ sở đào tạo như sau:")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/Screenshot_from_2023-08-24_19-52-23.png")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/Screenshot_from_2023-08-24_19-54-11_knv8M1p.png")

        return [AllSlotsReset()]


class ActionEmploymentSituationAfterGraduation(Action):
    def name(self) -> Text:
        return "action_get_employment_situation_after_graduation"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Kết quả khảo sát sinh viên có việc làm trong khoảng thời gian 12 tháng kể từ khi được công nhận tốt nghiệp được xác định theo từng ngành, lĩnh vực đào tạo, được khảo sát ở năm liền kề trước năm tuyển sinh, đối tượng khảo sát là sinh viên đã tốt nghiệp ở năm trước cách năm tuyển sinh một năm.")
        dispatcher.utter_message(text="CƠ SỞ ĐÀO TẠO PHÍA BẮC (BVH)")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/phia_bac_P2vob1D.png")
        dispatcher.utter_message(text="CƠ SỞ ĐÀO TẠO PHÍA NAM (BVS)")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/phia_nam_4E267Yn.png")

        return [AllSlotsReset()]


class ActionGetPriorityMode(Action):
    def name(self) -> Text:
        return "action_get_priority_mode"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Thông tin về điều kiện để được hưởng chế độ ưu tiên và các minh chứng cần có như sau:")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/368440270_1025767065461204_7815919778935244577_n.png")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/367539140_985253129417585_3649603965878251580_n.png")
        dispatcher.utter_message(image="https://chatbotdata.aisenote.com/media/images/368094431_252930061027056_3757731788816903322_n.png")
        dispatcher.utter_message(text="Đối với đối tượng 04, xem thêm thông tin về mẫu 05 ban hành kèm theo nghị định 131/2021/NĐ-CP tại: https://thuvienphapluat.vn/van-ban/Tai-chinh-nha-nuoc/Nghi-dinh-131-2021-ND-CP-huong-dan-Phap-lenh-Uu-dai-nguoi-co-cong-voi-cach-mang-288920.aspx")
        dispatcher.utter_message(text="Đối với đối tượng 07, xem thêm thông tin tại thông tư liên tịch số 37/2012/TTLT-BLĐTBXH-BYT-BTC-BGDĐT tại: https://thuvienphapluat.vn/van-ban/the-thao-y-te/thong-tu-lien-tich-37-2012-ttlt-bldtbxh-byt-btc-bgddt-xac-dinh-muc-do-khuyet-tat-179414.aspx")
        dispatcher.utter_message(text="Hoặc bạn có thể xem thêm thông tin tại: https://portal.ptit.edu.vn/thong-bao-v-v-thu-ho-so-xet-mien-giam-hoc-phi-va-ho-tro-chi-phi-hoc-tap-hoc-ky-1-nam-hoc-2023-2024/")
        return [AllSlotsReset()]


class ActionGetTime(Action):
    def name(self) -> Text:
        return "action_get_time"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        with open(root_path + "/crawl-data/time.json", 'r', encoding='utf8') as file:
            json_data = json.load(file)

        time_info_list = json_data.get("data")
        message = ""

        for item in time_info_list:
            time = item.get("mocThoiGian")
            parsed_date = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")
            formatted_date = parsed_date.strftime("%d-%m-%Y")
            message += f"\n {item.get('tieuDe')}: {formatted_date}"
        message += "\n TUYỂN SINH BỔ SUNG ĐỢT 2: sẽ có thông báo riêng trước 15 ngày thí sinh đăng ký xét tuyển."
        dispatcher.utter_message(text=message)
        return [AllSlotsReset()]


class ActionGetClub(Action):
    def name(self) -> Text:
        return "action_get_club"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        with open(root_path + "/crawl-data/clb.json", 'r', encoding='utf8') as file:
            json_data = json.load(file)
            club_info_list = json_data.get("data")

        dispatcher.utter_message(text="Hiện tại có 11 câu lạc bộ nhóm sinh viên:")

        for item in club_info_list:
            name = item.get("name")
            leader = item.get("leader")
            fanpage = item.get("fanpage")
            dispatcher.utter_message(text=f"CLB: {name} \n Người phụ trách: {leader} \n Fanpage: {fanpage}")

        return [AllSlotsReset()]