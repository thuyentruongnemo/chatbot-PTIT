import requests
import json


def request_get(url, file_path):
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.9'
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        with open(file_path, 'w', encoding='utf-8') as json_file:
            json.dump(data, json_file, ensure_ascii=False, indent=4)
    else:
        print("Failed to fetch data. Status code:", response.status_code)


url = "https://apiquanlydaotao.ptit.edu.vn/he-dao-tao?page=1&limit=1000"
file_path = "data.json"
request_get(url, file_path)

url = "https://apiquanlydaotao.ptit.edu.vn/chi-tieu/thi-sinh/co-so/BVH?nam=2023"
file_path = "bvh.json"
request_get(url, file_path)

url = "https://apiquanlydaotao.ptit.edu.vn/chi-tieu/thi-sinh/co-so/BVS?nam=2023"
file_path = "bvs.json"
request_get(url, file_path)

url = "https://apiquanlydaotao.ptit.edu.vn/moc-thoi-gian-quan-trong?sort=mocThoiGian&order=1"
file_path = "time.json"
request_get(url, file_path)

url = "https://apiquanlydaotao.ptit.edu.vn/hoc-bong?sort=thuTu&order=1"
file_path = "scholarship-policy.json"
request_get(url, file_path)
