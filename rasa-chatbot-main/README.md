# Chatbot with ReactJS, Rasa, and Socket.IO

This project is an example of integrating a chatbot into a web application using ReactJS and Rasa with real-time communication via Socket.IO.

## Requirements

1. [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/)
2. [Rasa](https://rasa.com/docs/rasa/installation/) - Chatbot framework
3. [Socket.IO](https://socket.io/) - Real-time communication library
4. python version 3.8+

## Clone project

1. Clone the project: git clone https://gitlab.com/ait12/rasa-chatbot.git

## Install dependencies
1. Reactjs:
   - cd site
   - npm install
2. Rasa:
   - cd rasa-core
   - pip install -r requirements.txt


## Usage

1. Start the Rasa server: rasa run -m models --enable-api --cors "*"
2. Start Actions server: rasa run actions
3. Start the React application server: npm run dev
4. Access the application at `http://localhost:5173`.

## License

This project is distributed under the MIT License. See details in the LICENSE file.



